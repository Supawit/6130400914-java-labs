/* This program draws 2D Graphic.
 * Extends MyCanvas.
 * Use MyBall ,MyBrick ,MyPedal class to draw. 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV2 extends MyCanvas {
	MyBall ball;
	MyBrick brick;
	MyPedal pedal;
	
	@Override 
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		Line2D.Double vertical = new Line2D.Double(WIDTH/2, HEIGHT, WIDTH/2, 0);
		Line2D.Double horizontal = new Line2D.Double(WIDTH, HEIGHT/2, 0, HEIGHT/2);
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.WHITE);
		ball = new MyBall(385, 285);
		g2d.fill(ball);
		brick = new MyBrick(360, 0);
		g2d.fill(brick);
		pedal = new MyPedal(350, 590);
		g2d.fill(pedal);
		g2d.draw(vertical);
		g2d.draw(horizontal);
	}
}
