/* This program draws 2D Graphic.
 * Extends MyCanvasV3.
 * Have array brick[] and brickrow[].
 * brick[] contain 10 object MyBrick.
 * brickrow[] contain 7 color.
 * Use 2 for loops to creat brick and brickrow.
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

public class MyCanvasV4 extends MyCanvasV3{
	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		MyPedal simplePedal = new MyPedal(0, 0);
		MyBrick simpleBrick = new MyBrick(0, 0);
		MyBall simpleBall = new MyBall(0, 0);
		MyBrick brick[] = new MyBrick[10];
		Color brickrow[] = new Color[7];
		brickrow[0] = Color.RED;
		brickrow[1] = Color.ORANGE;
		brickrow[2] = Color.YELLOW;
		brickrow[3] = Color.GREEN;
		brickrow[4] = Color.BLUE;
		brickrow[5] = Color.MAGENTA;
		brickrow[6] = Color.PINK;
		
		g2d.setStroke(new BasicStroke(6));
		int x = 0;
		int y = 200;
		
		for (int i = 0; i < brickrow.length; i++) {
			for (int j = 0; j < brick.length; j++) {
				g2d.setColor(brickrow[6 - i]);
				brick[j] = new MyBrick(x, y);
				x += simpleBrick.brickWidth;
				g2d.fill(brick[j]);
				g2d.setColor(Color.BLACK);
				g2d.draw(new Line2D.Double(x, y, x, y + simpleBrick.brickHeight));
			}
			g2d.setColor(Color.BLACK);
			g2d.draw(new Line2D.Double(0, y, WIDTH, y));
			x = 0;
			y -= simpleBrick.brickHeight;
		}
	    g2d.draw(new Line2D.Double(0, 0, 0, HEIGHT));
	    
	    g2d.setColor(Color.GRAY);
	    pedal = new MyPedal(WIDTH/2 - simplePedal.pedalWidth/2, HEIGHT - simplePedal.pedalHeight);
	    g2d.fill(pedal);
	    g2d.setColor(Color.WHITE);
	    ball = new MyBall(WIDTH/2 - simpleBall.diameter/2, HEIGHT - simpleBrick.brickWidth/2);   
	    g2d.fill(ball);
	}

}
