/* This program draws 2D Graphic.
 * Extends MyCanvasV2
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2{
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		MyBall simpleBall = new MyBall(0, 0);
		MyBall ball[] = new MyBall[4];
		ball[0] = new MyBall(0, 0);
		ball[1] = new MyBall(0, HEIGHT - simpleBall.diameter);
		ball[2] = new MyBall(WIDTH - simpleBall.diameter, 0);
		ball[3] = new MyBall(WIDTH - simpleBall.diameter, HEIGHT - simpleBall.diameter);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball[0]);
		g2d.fill(ball[1]);
		g2d.fill(ball[2]);
		g2d.fill(ball[3]);
		
	}
}
