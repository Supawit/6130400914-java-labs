/* This program draws 2D Graphic.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import javax.swing.JFrame;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

public class MyCanvas extends JPanel {
	public final static int WIDTH  = 800;
	public final static int HEIGHT = 600;
	protected Ellipse2D.Double circle = new Ellipse2D.Double(250, 150, 300, 300);
	protected Ellipse2D.Double oval1  = new Ellipse2D.Double(330, 255, 30 , 60);
	protected Ellipse2D.Double oval2  = new Ellipse2D.Double(430, 255, 30 , 60);
	protected Rectangle2D.Double rectangle   = new Rectangle2D.Double(350, 375, 100, 10);
	

	public MyCanvas() {
		//super();
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setBackground(Color.BLACK);
	}
	
	@Override 
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.WHITE);
		g2d.draw(circle);
		g2d.fill(oval1);
		g2d.fill(oval2);
		g2d.fill(rectangle);
	}


	
}
