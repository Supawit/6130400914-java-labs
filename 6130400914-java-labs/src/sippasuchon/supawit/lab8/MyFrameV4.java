/* This program creates Frame name "My Frame4" extends MyFrame3.
 * Create object from MyCanvas4 class.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3{

	public MyFrameV4(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});

			}
			public static void createAndShowGUI() {
				MyFrameV4 msw = new MyFrameV4("My Frame V4");
				msw.addComponents();
				msw.setFrameFeatures();
			}
			
			protected void addComponents() {
				add(new MyCanvasV4());
			}
			
			protected void setFrameFeatures() {
				pack();
				setLocationRelativeTo(null);
				setVisible(true);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			}

}
