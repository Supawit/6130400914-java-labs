/*This program creates 2D Rectangle with width = 80 and height = 20.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import java.awt.geom.*;

public class MyBrick extends Rectangle2D.Double{
	public  final static int brickWidth = 80;
	public  final static int brickHeight = 20;
	 
	public MyBrick(double arg0, double arg1) {
		super(arg0, arg1, brickWidth, brickHeight);
		// TODO Auto-generated constructor stub
	}
	
}
