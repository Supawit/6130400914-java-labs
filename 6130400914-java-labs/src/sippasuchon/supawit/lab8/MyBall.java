/*This program creates 2D Ball with diameter 30.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import java.awt.geom.Ellipse2D;

public class MyBall extends Ellipse2D.Double{
	public  final static int diameter = 30;

	public MyBall(double arg0, double arg1) {
		super(arg0, arg1, diameter, diameter);
		// TODO Auto-generated constructor stub
	}

	
}
