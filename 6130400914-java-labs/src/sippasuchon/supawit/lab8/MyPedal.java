/*This program creates 2D pedal with width 100 and height 10.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:March 28 2019
 */
package sippasuchon.supawit.lab8;

import java.awt.geom.*;

public class MyPedal extends Rectangle2D.Double{
	public final static int pedalWidth = 100;
	public final static int pedalHeight = 10;
	 
	public MyPedal(double arg0, double arg1) {
		super(arg0, arg1, pedalWidth, pedalHeight);
		// TODO Auto-generated constructor stub
	}

}
