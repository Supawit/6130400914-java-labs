/* Save and load data.
 *  
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:May 05 2019
 */
package sippasuchon.supawit.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Collections;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import sippasuchon.supawit.lab10.MobileDeviceFormV9;
import sippasuchon.supawit.lab5.MobileDevice;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class MobileDeviceFormV12  extends MobileDeviceFormV11 implements ActionListener{
	protected String fileName;
	protected FileOutputStream fileOutput;
	protected ObjectOutputStream dataOutput;
	protected FileInputStream fileInput = null;
	protected ObjectInputStream dataInput = null;
	
	public MobileDeviceFormV12(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV12 mobileDeviceFrom12 = new MobileDeviceFormV12("Mobile Device Form V12");
		mobileDeviceFrom12.addComponents();
		mobileDeviceFrom12.addMenus();
		mobileDeviceFrom12.addListeners();
		mobileDeviceFrom12.enableKeyboard();
		mobileDeviceFrom12.setFrameFeatures();	
	}

	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();
		if(src == saveItem) {
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				try {
					File file = fc.getSelectedFile();
				 	fileName = file.getName();
				 	
					fileOutput = new FileOutputStream(fileName);
					dataOutput = new ObjectOutputStream(fileOutput);
	
					for(MobileDevice information : mobileInfo) {
						dataOutput.writeObject(information);
					}
					dataOutput.close();
					fileOutput.close();
				} catch (IOException ex){
					System.err.println("can't find");
				} catch (Exception ex) {
					System.err.println("Eror");
				}
			}
		} else if (src == openItem){
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				mobileInfo.removeAll(mobileInfo);
				try {
					File file = fc.getSelectedFile();
				 	fileName = file.getName();
					fileInput = new FileInputStream(fileName);
					dataInput = new ObjectInputStream(fileInput);
					try {
						while(true) {
							MobileDevice mobile = (MobileDevice) dataInput.readObject();
							mobileInfo.add(mobile);
						}
					} catch (IOException ex) {
						dataOutput.close();
						fileOutput.close();
					}
				} catch (Exception ex) {
					
				}
			}
		}
	}
}
