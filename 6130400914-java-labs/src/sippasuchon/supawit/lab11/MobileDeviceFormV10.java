/* 1. Adding items to the list.
 * 2. Display the list.
 * 3. Sorting the list.
 * 4. Searching an item the list.
 * 5. Removing an item from the list.
 *
 *
 *
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 26 2019
 */
package sippasuchon.supawit.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import sippasuchon.supawit.lab10.MobileDeviceFormV9;
import sippasuchon.supawit.lab5.MobileDevice;

public class MobileDeviceFormV10 extends MobileDeviceFormV9 implements ActionListener{
	//private ArrayList<String> mobileInformation = new ArrayList<String>();
	protected ArrayList<MobileDevice> mobileInfo = new ArrayList<MobileDevice>();
	protected MobileDevice mobile;
	protected int price = 0;
	protected int weight = 0;
	protected JMenu menuData;
	protected JMenuItem displayItem;
	protected JMenuItem sortItem;
	protected JMenuItem searchItem;
	protected JMenuItem removeItem;

	
	
	public MobileDeviceFormV10(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				
			}
		});		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV10 mobileDeviceFrom10 = new MobileDeviceFormV10("Mobile Device Form V10");
		mobileDeviceFrom10.addComponents();
		mobileDeviceFrom10.addMenus();
		mobileDeviceFrom10.addListeners();
		mobileDeviceFrom10.enableKeyboard();
		mobileDeviceFrom10.setFrameFeatures();	
		
	}
	
	protected void addMenus() {
		super.addMenus();
		menuData = new JMenu("Data");
		
		displayItem = new JMenuItem("Display");
		sortItem = new JMenuItem("Sort");
		searchItem = new JMenuItem("Search");
		removeItem = new JMenuItem("Remove");
		
		menuData.add(displayItem);
		menuData.add(sortItem);
		menuData.add(searchItem);
		menuData.add(removeItem);
		menuBar.add(menuData);
	}
		
	protected void addListeners() {
		super.addListeners();
		displayItem.addActionListener(this);
		sortItem.addActionListener(this);
		searchItem.addActionListener(this);
		removeItem.addActionListener(this);
		menuData.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {	
		
		Object src = e.getSource();
	
		if (src == okBut) {	
			add();		
		} else if (src == displayItem) {
			int i = 1;
			String text = " ";
			for(MobileDevice information : mobileInfo) {
				text += i + ": " + information + "\n ";
				i ++;
			}
			JOptionPane.showMessageDialog(null, text);
		} else if(src == sortItem){
			Collections.sort(mobileInfo, new SortPrice());
			
		} else if (src == searchItem) {
			String message = JOptionPane.showInputDialog("Please input model name to search: ");
			boolean find = true;
			for(MobileDevice information : mobileInfo) {
				if (message.equals(information.getModelName())) {
					JOptionPane.showMessageDialog(null, information + "\nis found" );
					find = false;
				} 
			}
			if (find) {
				JOptionPane.showMessageDialog(null, message + " is NOT found");
			}
			
		} else if (src == removeItem) {
			String message = JOptionPane.showInputDialog("Please input model name to remove: ");
			boolean find = true;
			for(MobileDevice information : mobileInfo) {
				if (message.equals(information.getModelName())) {
					JOptionPane.showMessageDialog(null, information + "\nis removed." );
					mobileInfo.remove(information);
					find = false;
					break;
				} 
			}
			if (find) {
				JOptionPane.showMessageDialog(null, message + " is NOT found");
			}
		} else {
			super.actionPerformed(e);
		}
		
			
		
	}
		
	public void add() {
		mobile = new MobileDevice(modelText.getText(), textRadioButton, price, weight);
		mobileInfo.add(mobile);
		System.out.println(mobileInfo);
	}
	
}

