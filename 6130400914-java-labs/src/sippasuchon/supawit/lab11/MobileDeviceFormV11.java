/* Check an empty string.
 * If the user presses the button "OK" of the mobile device form window, 
 *  the program checks whether an model name is entered or not.
 * If it is not entered, the program will display the message as "Please enter model name" .
 *  
 * Check a valid number
 * If the user presses the button "OK" of the mobile device form window, 
 *  the program checks whether the entered weight for the weight field is valid or not. 
 * If it is not a numeric input, the program will display a message "Please enter only numeric input for weight".
 *  
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:May 05 2019
 */
package sippasuchon.supawit.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import sippasuchon.supawit.lab5.MobileDevice;

public class MobileDeviceFormV11 extends MobileDeviceFormV10 implements ActionListener{
	protected int minWeight = 100;
	protected int maxWeight = 3000;
	
	public MobileDeviceFormV11(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV11 mobileDeviceFrom11 = new MobileDeviceFormV11("Mobile Device Form V11");
		mobileDeviceFrom11.addComponents();
		mobileDeviceFrom11.addMenus();
		mobileDeviceFrom11.addListeners();
		mobileDeviceFrom11.enableKeyboard();
		mobileDeviceFrom11.setFrameFeatures();	
	}
	
	
	public void actionPerformed(ActionEvent e) {	
		Object src = e.getSource();
		boolean num = true;
		boolean model = true;
		boolean weightCheck = true;
		boolean priceCheck  = true;
		if (src == okBut) {	
			try {
				price = Integer.parseInt(priceText.getText());
			} catch (NumberFormatException e2) {
				
			}
			
			try {
				weight = Integer.parseInt( weightText.getText());
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(null, "Please enter only numeric in put for weight");
				num = false;
			}
			
			if(modelText.getText().isEmpty() || modelText.getText() == null) {
				JOptionPane.showMessageDialog(null, "Please enter model name");
				model = false;
			}
			
			if(minWeight - 1 >= weight) {
				JOptionPane.showMessageDialog(null, "Too light:valid weight is[" + minWeight + "," +maxWeight + "]");
				weightCheck = false;
			} else if (maxWeight + 1 <= weight){
				JOptionPane.showMessageDialog(null, "Too heavy:valid weight is[" + minWeight + "," +maxWeight + "]");
				weightCheck = false;
			} 
			
			if (num && model && weightCheck) {	
				add();
			} 
		} else {
			super.actionPerformed(e);
		}				
	}	
}
