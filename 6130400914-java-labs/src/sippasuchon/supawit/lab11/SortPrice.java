package sippasuchon.supawit.lab11;

import java.util.Comparator;

import sippasuchon.supawit.lab5.MobileDevice;

public class SortPrice implements Comparator<MobileDevice> {

	@Override
	public int compare(MobileDevice o1, MobileDevice o2) {
		// TODO Auto-generated method stub
		return o1.getPrice() < o2.getPrice() ? -1 :o1.getPrice() == o2.getPrice() ? 0 : 1;
	}

}
