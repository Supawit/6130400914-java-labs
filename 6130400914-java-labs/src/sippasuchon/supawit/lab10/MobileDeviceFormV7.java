/* When user click OK button.The dialog shows text is all text fields, selected value in the radio button,
   		chosen value in the combo box, selected items in the list, and text in the text area.
 * When a user presses button Cancel, the program resets all text fields and text area. 
 * When the combo box changes, the option message dialog shows the update value.
 * When the radio button changes, the option message dialog shows the update value.
 * When the features list changes, the option message dialog shows the update value.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 21 2019
 */
package sippasuchon.supawit.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sippasuchon.supawit.lab7.MobileDeviceFormV6;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener , ListSelectionListener {
	protected String textRadioButton = "Android";
	protected String textList = "Design and build quality,Great Camera,Battery Life";
	protected String phoneBoxText = "Phone";
	public MobileDeviceFormV7(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				
			}
		});		
	}
	
		public static void createAndShowGUI() {
		MobileDeviceFormV7 mobileDeviceFrom7 = new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceFrom7.addComponents();
		mobileDeviceFrom7.addMenus();
		mobileDeviceFrom7.addListeners();
		mobileDeviceFrom7.setFrameFeatures();	

	}
		
	protected void addComponents() {
		super.addComponents();
		android.setSelected(true);
		labelList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);	
		int[] select = {0, 1, 3};
		labelList.setSelectedIndices(select);
		
	}
	


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object src = e.getSource();
		if (src == okBut) {
			JOptionPane.showMessageDialog(null, "Brand Name:  " + brandText.getText() + ", Model Name:  " + modelText.getText() +
					", Weight:  " + weightText.getText() + ", Price:  " + priceText.getText() + 
					"\n" + "Os:  " + textRadioButton + "\nType:  " + phoneBoxText + "\nFeatures:  " + textList
					+ "\nReview:" + reviewTextArea.getText());
			
		} else if (src == cancelBut) {
			brandText.setText(null);
			modelText.setText(null);
			weightText.setText(null);
			priceText.setText(null);
			reviewTextArea.setText(null);
		}
		if (src == phoneBox){
			phoneBoxText = (String) phoneBox.getSelectedItem();
			JOptionPane.showMessageDialog(null, "Type is updated to " + phoneBox.getSelectedItem());
		}
	

	}
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub	
		JRadioButton source = (JRadioButton) e.getItemSelectable();
		
		if (e.getStateChange() == ItemEvent.SELECTED) {		
			textRadioButton = source.getText();
			JOptionPane.showMessageDialog(null, "Your os platform is now changed to " + textRadioButton);
		}
		
	}
	
		@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		JList source = (JList) e.getSource();
		if (source.isSelectionEmpty()) {	
			textList = "";
		}  else {
			textList = "";
			int j = 0;
			for(int i = 0; i <= source.getMaxSelectionIndex(); i++) {
				if (source.isSelectedIndex(i)) {
					j++;
					if (j != 1) {
						textList += ", ";
					} 
					textList += label[i];
					
				}
			}
		}
		JOptionPane.showMessageDialog(null, textList);	
	}
		
	protected void addListeners() {
		okBut.addActionListener(this);
		cancelBut.addActionListener(this);
		android.addItemListener(this);
		ios.addItemListener(this);
		labelList.addListSelectionListener(this);
		phoneBox.addActionListener(this);
	}



}
