/* When user chooses the Open menu item, open file chooser dialog is displayed.
 *    And after user choose a file to open and press Open button,  the program show a dialog showing the chosen file has been opened.
 *    If user chooses Cancel button instead, the program displays a message "Open command cancelled by user".
 * When user chooses the Save menu item, save file dialog is displayed. 
 *	  After user chooses a file or types in file name to be saved and press Save button, a message with a saved file name is displayed . 
 * 	  If user chooses Cancel button instead, the program displays a message "Save command cancelled by user".
 * When user chooses the Exit menu item, the program will exit.
 * When user chooses the menu items Red, Green, Blue, the program will change the text color as the user selects.
 * When the user chooses menu Custom color, the program will open the color chooser and change the text color as the user selects.  
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 21 2019
 */
package sippasuchon.supawit.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
	protected JFileChooser fc = new JFileChooser();
	protected int returnVal;
	public MobileDeviceFormV9(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	} 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				
			}
		});		
	}
	
		public static void createAndShowGUI() {
		MobileDeviceFormV9 mobileDeviceFrom9 = new MobileDeviceFormV9("Mobile Device Form V9");
		mobileDeviceFrom9.addComponents();
		mobileDeviceFrom9.addMenus();
		mobileDeviceFrom9.addListeners();
		mobileDeviceFrom9.enableKeyboard();
		mobileDeviceFrom9.setFrameFeatures();	

	}
		
	protected void addComponents() {
		super.addComponents();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();
		if(src == openItem) {
			returnVal = fc.showOpenDialog(MobileDeviceFormV9.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Opening file " + file.getName());
			} else if(returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Open command cancelled by user");
			}	
		} else if (src == saveItem) {
			returnVal = fc.showSaveDialog(MobileDeviceFormV9.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Save file " + file.getName());
			} else if(returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Save command cancelled by user");
			}	
		} else if (src == menuCustom) {
			Color newColor = JColorChooser.showDialog(null, "Choose color", Color.RED);
			reviewTextArea.setBackground(newColor);
		} else if (src == redItem) {
			reviewTextArea.setBackground(Color.RED);
		} else if (src == blueItem) {
			reviewTextArea.setBackground(Color.BLUE);
		} else if (src == greenItem) {
			reviewTextArea.setBackground(Color.GREEN);
		} else if (src == exitItem) {
			System.exit(0);
		}
		
	}
}
