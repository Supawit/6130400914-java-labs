/* Set accelerators and mnemonic keys.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 21 2019
 */
package sippasuchon.supawit.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import sippasuchon.supawit.lab7.MobileDeviceFormV6;

public class MobileDeviceFormV8 extends MobileDeviceFormV7{
	protected JMenuItem menuCustom;

	
	public MobileDeviceFormV8(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				
			}
		});		
	}
	
		public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceFrom8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceFrom8.addComponents();
		mobileDeviceFrom8.addMenus();
		mobileDeviceFrom8.addListeners();
		mobileDeviceFrom8.enableKeyboard();
		mobileDeviceFrom8.setFrameFeatures();	

	}
		
	protected void addComponents() {
		super.addComponents();
		
	}
	protected void addMenus() {
		super.addMenus();
		menuCustom = new JMenuItem("Custom ...");
		subCorlorMenu.add(menuCustom);
		
	}
	
	protected void enableKeyboard() {
		
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuFile.addActionListener(this);
		setMAKeys(newItem, KeyEvent.VK_N, KeyEvent.VK_N, this);
		setMAKeys(openItem, KeyEvent.VK_O, KeyEvent.VK_O, this);
		setMAKeys(saveItem, KeyEvent.VK_S, KeyEvent.VK_S, this);
		setMAKeys(exitItem, KeyEvent.VK_X, KeyEvent.VK_X, this);
		menuConfig.setMnemonic(KeyEvent.VK_C);
		menuConfig.addActionListener(this);
		subCorlorMenu.setMnemonic(KeyEvent.VK_L);
		subCorlorMenu.addActionListener(this);
		setMAKeys(blueItem, KeyEvent.VK_B, KeyEvent.VK_B, this);
		setMAKeys(greenItem, KeyEvent.VK_G, KeyEvent.VK_G, this);
		setMAKeys(redItem, KeyEvent.VK_R, KeyEvent.VK_R, this);
		setMAKeys(menuCustom, KeyEvent.VK_U, KeyEvent.VK_U, this);
		

	}
	
	protected void setMAKeys(JMenuItem menu, int mKey, int aKey, ActionListener listner) {
		menu.setMnemonic(mKey);
		menu.setAccelerator(KeyStroke.getKeyStroke(aKey, ActionEvent.CTRL_MASK));
		menu.addActionListener(listner);
	}

}
