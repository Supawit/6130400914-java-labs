package sippasuchon.supawit.lab7;

import javax.swing.*;
import java.awt.*;

public class MySimpleWindow extends JFrame {
	protected JPanel panelBot = new JPanel();
	protected JButton cancelBut = new JButton();
	protected JButton okBut = new JButton();
			
	public MySimpleWindow(String string) {
		super (string);
	}
	protected void addComponents() {
		cancelBut = new JButton("Cancel");
		okBut = new JButton("OK");
		panelBot.add(cancelBut);
		panelBot.add(okBut);
		setLayout(new BorderLayout());
		add(panelBot, BorderLayout.SOUTH);
		
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void createAndShowGUI() {
			MySimpleWindow msw = new MySimpleWindow("My Simple Window");
			msw.addComponents();
			msw.setFrameFeatures();	
	}

	
		public static void main(String[] args) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});
		}
}