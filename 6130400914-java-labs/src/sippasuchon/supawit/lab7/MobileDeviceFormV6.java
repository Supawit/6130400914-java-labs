package sippasuchon.supawit.lab7;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
	protected JLabel image;
	protected JPanel topBotPanel;
	protected JPanel botBotPanel;
	
	public MobileDeviceFormV6(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceFrom6 = new MobileDeviceFormV6("Mobile Device Form V6");
		//MyDrawPanel imageex = new MyDrawPanel();
		mobileDeviceFrom6.addComponents();
		mobileDeviceFrom6.addMenus();
		mobileDeviceFrom6.setFrameFeatures();	

	}
	
	protected void addComponents() {
		super.addComponents();
		
		panelBot.removeAll();
		image = new JLabel(new ImageIcon("images/galaxyNote9.png"));
		panelBot.add(image);
		
		topBotPanel = new JPanel();
		botBotPanel = new JPanel();
		
		topBotPanel.add(image);
		botBotPanel.add(cancelBut);
		botBotPanel.add(okBut);
		
		panelBot.setLayout(new BorderLayout());
		panelBot.add(topBotPanel, BorderLayout.NORTH);
		panelBot.add(botBotPanel, BorderLayout.SOUTH);
		
		
		

		
		
	}
	 
}
