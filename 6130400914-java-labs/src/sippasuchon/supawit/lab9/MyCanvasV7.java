/* This program draws 2D Graphic.
 * Extends MyCanvasV6 and implements Runnable.
 * Bounce ball when hit brick and make brick disappearing.
 * There is 1 method checkCollision().
 * Method checkCollision() check if ball hit brick and make brick disappearing.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import sippasuchon.supawit.lab8.MyBall;
import sippasuchon.supawit.lab8.MyBrick;

public class MyCanvasV7 extends MyCanvasV6 implements Runnable{
	protected int numBricks = WIDTH/MyBrick.brickWidth;
	Thread running3 = new Thread(this);
	protected MyBrickV2 brick[] = new MyBrickV2[numBricks]; 
	//MyBrickV2 delBrick;
	int blackBrick = -1;
	
	
	public MyCanvasV7() {
		super();
		 ball = new MyBallV2(0, 0);
		 ball.ballVelX = 2;
		 ball.ballVelY = 2;
		 for (int i = 0; i < numBricks; i++) {
			 brick[i] = new  MyBrickV2(MyBrick.brickWidth * i, HEIGHT/2);
		 }
		 running3.start();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, WIDTH, HEIGHT));
		g2d.setStroke(new BasicStroke(6));

		for (int i = 0; i < numBricks; i++) {

			if(brick[i].visible) {
				g2d.setColor(Color.RED);
				g2d.draw(brick[i]);
				g2d.setColor(Color.BLUE);
				g2d.fill(brick[i]);
			}
				
		
			g2d.setColor(Color.WHITE);
			g2d.fill(ball);
			
		 }
		
		
	}
	@Override
	
	public void run() {
		while(true) {
		// TODO Auto-generated method stub
			if (ball.x > WIDTH - MyBall.diameter ) {
				ball.ballVelX *= -1;
				ball.x = WIDTH - MyBall.diameter ;
			} else if (ball.x < 0) {
				ball.ballVelX *= -1;
				ball.x = 0;
			}
			
			if (ball.y > HEIGHT - MyBall.diameter) {
				ball.ballVelY *= -1;
				ball.y = HEIGHT - MyBall.diameter;
			} else if (ball.y < 0){
				ball.ballVelY *= -1;
				ball.y = 0;
			}
			
			for(int i = 0; i < numBricks; i++) {
				if(brick[i].visible) {
					checkCollision(ball, brick[i]);
				}
			}
			
			ball.move();
			repaint();
			
			try{
				Thread.sleep(10);
			} catch (InterruptedException ex) {
			
			}
		
		}
	}
	
	void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter/2.0;
		double y = ball.y + MyBall.diameter/2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter)/4.0;
		
		if (collided) {
			if(deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
		
			ball.move();
			brick.visible = false;
		}
	
	}
	
}
