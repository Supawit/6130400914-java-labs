/* This Class extends MyPedal.
 * There are 2 method moveLeft() and moveRight().
 * Method moveLeft() will change x when use this method but 
 	if x - speedPedal < 0 set x = 0.
 * Method moveRight() will change x  when use this method but 
 	if 5*speedPedal + x > MyCanvas.WIDTH set x =  MyCanvas.WIDTH - MyPedal.pedalWidth.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import sippasuchon.supawit.lab8.MyCanvas;
import sippasuchon.supawit.lab8.MyPedal;

public class MyPedalV2 extends MyPedal {
	final static int speedPedal = 20;
	
	public MyPedalV2(double arg0, double arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	
	public void moveLeft() {
		x -= speedPedal;
		if (x - speedPedal < 0){
			x = 0;
		} 
	}
	
	public void moveRight() {
		x += speedPedal;
		if (5*speedPedal + x > MyCanvas.WIDTH) {
			
			x = MyCanvas.WIDTH - MyPedal.pedalWidth;
		} 
	}
}
