/* This program draws 2D Graphic.
 * Extends MyCanvasV5 and implements Runnable.
 * Bounce ball when hit.
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import sippasuchon.supawit.lab8.MyBall;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable{
	MyBallV2 ball2 = new MyBallV2(WIDTH/2, HEIGHT/2);
	Thread running2;

	public MyCanvasV6() {
		super();
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running2 = new Thread(this);
		running2.start();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, super.WIDTH, super.HEIGHT);
		g2d.setColor(Color.WHITE);  
		g2d.fill(ball2);
	}
	
	@Override
	public void run() {
		while(true) {
		// TODO Auto-generated method stub
			if (ball2.x > WIDTH - MyBall.diameter ) {
				ball2.ballVelX *= -1;
				ball2.x = WIDTH - MyBall.diameter ;
			} else if (ball2.x < 0) {
				ball2.ballVelX *= -1;
				ball2.x = 0;
			}
			
			if (ball2.y > HEIGHT - MyBall.diameter) {
				ball2.ballVelY *= -1;
				ball2.y = HEIGHT - MyBall.diameter;
			} else if (ball2.y < 0){
				ball2.ballVelY *= -1;
				ball2.y = 0;
			}
			ball2.move();		
			repaint();
		
			try{
				Thread.sleep(5);
			} catch (InterruptedException ex) {
			
			}
		
		}
	}
}
