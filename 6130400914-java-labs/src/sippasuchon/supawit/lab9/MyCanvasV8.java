 /* This program draws 2D Graphic.
 * Extends MyCanvasV7 and implements Runnable, KeyListener.
 * Build bricks.
 * There are 2 method collideWithPedal and checkPassBottom.
 * collideWithPedal make ball hit with pedal.
 * checkPassBottom check if ball fall below pedal -1 lives and set ball to mid of pedal.
 * If lives = 0 .Program will display text "GAME OVER".
 * If User plays and hits all bricks .Program will display text "You win".
 * Press left arrow to move pedal left.
 * Press right arrow to move pedal right.
 * Press spacebar to release ball.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import sippasuchon.supawit.lab8.MyBall;
import sippasuchon.supawit.lab8.MyBrick;
import sippasuchon.supawit.lab8.MyCanvas;
import sippasuchon.supawit.lab8.MyPedal;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener{
	protected int numCol = WIDTH/MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks = new MyBrickV2[numRow][numCol]; 
	protected Thread running;
	protected Color[] color = {Color.MAGENTA, Color.BLUE, Color.CYAN,
			Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED};
	protected MyPedalV2 pedal;
	protected int lives;
	protected boolean space = false;
	
	public MyCanvasV8() {
		super();
		ball = new MyBallV2(WIDTH/2 - MyBall.diameter/2, HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;
		
		running = new Thread(this);
		
		for(int i = 0; i < numRow; i++) {
			for(int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, HEIGHT/3 - i*MyBrick.brickHeight);
			}
		}
				
		running.start();
			
		setFocusable(true);
		addKeyListener(this);
		
		pedal = new MyPedalV2(WIDTH/2 - MyPedal.pedalWidth/2, HEIGHT - MyPedal.pedalHeight);
		lives = 3 ;
	}
		
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, WIDTH, HEIGHT));
		int invisibleBricks = 0;
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if(bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.BLACK);
					g2d.draw(bricks[i][j]);
				}
				if(bricks[i][j].visible == false) {
					invisibleBricks += 1;
				}
			}

			g2d.setColor(Color.WHITE);
			g2d.fill(ball);
			
			g2d.setColor(Color.GRAY);
			g2d.fill(pedal);
			
		
			g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
			g2d.setColor(Color.BLUE);
			String s = " Live : " + lives;
			g2d.drawString(s, 10, 30);
			
			
			if(invisibleBricks == 70) {
				g2d.setFont(new Font("SanSerif", Font.BOLD, 60));
				g2d.setColor(Color.GREEN);
				String win = "You won";
				g2d.drawString(win, HEIGHT/2 - 60, WIDTH/2 - 60);
			}
				
			if(lives == 0) {
				g2d.setFont(new Font("SanSerif", Font.BOLD, 60));
				g2d.setColor(Color.GRAY);
				String over = "GAME OVER";
				g2d.drawString(over, HEIGHT/2 - 60, WIDTH/2 - 60);
					
			}
		
		}
	}
		
		
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		    if(arg0.getKeyCode() == 39) {
		    	pedal.moveRight();	
		    			
			} else if (arg0.getKeyCode() == 37) {
				pedal.moveLeft();
			
			} else if (arg0.getKeyCode() == KeyEvent.VK_SPACE) {
				int random;	
				random = (int)(Math.random() * 2 + 1);
				if (lives > 0 ) {
					if (random == 1){
						ball.ballVelX = 4;
					} else {
						ball.ballVelX = -4; 
					}
					ball.ballVelY = 4;
				}
			
			}
		    
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void run() {
		while(true) {
		// TODO Auto-generated method stub
			if (ball.x > WIDTH - MyBall.diameter ) {
				ball.ballVelX *= -1;
				ball.x = WIDTH - MyBall.diameter ;
			} else if (ball.x < 0) {
				ball.ballVelX *= -1;
				ball.x = 0;
			}
			if (ball.y < 0){
				ball.ballVelY *= -1;
				ball.y = 0;
			}
			

			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if(bricks[i][j].visible) {
						checkCollision(ball, bricks[i][j]);
					}
				}
			}
			
			collideWithPedal(ball, pedal);
			checkPassBottom(ball);
			
			ball.move();
			repaint();
			
			
			try{
				Thread.sleep(20);
			} catch (InterruptedException ex) {
			
			}
		
		}
			
	}
	
	void collideWithPedal(MyBallV2 ball, MyPedalV2 pedal) {
		double x = ball.x + MyBall.diameter/2.0;
		double y = ball.y + MyBall.diameter/2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyBrick.brickHeight));
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter)/4.0;
		
		if (collided) {
			if(deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
		}
	}
	
	void checkPassBottom(MyBallV2 ball) {
		if(ball.y >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.pedalWidth/2 - MyBall.diameter/2;
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}
	
	
	
}

