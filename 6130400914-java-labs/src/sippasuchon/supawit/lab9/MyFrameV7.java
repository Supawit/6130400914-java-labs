/* This program creates Frame name "My Frame7" extends MyFrame6.
 * Create object from MyCanvas7 class.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6{

	public MyFrameV7(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});

			}
			public static void createAndShowGUI() {
				MyFrameV7 msw = new MyFrameV7("My Frame V7");
				msw.addComponents();
				msw.setFrameFeatures();
			}
			
			protected void addComponents() {
				add(new MyCanvasV7());
			}
			
			protected void setFrameFeatures() {
				pack();
				setLocationRelativeTo(null);
				setVisible(true);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			}

}
