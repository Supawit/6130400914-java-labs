/* This program creates Frame name "My Frame8" extends MyFrame7.
 * Create object from MyCanvas8 class.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV8 extends  MyFrameV7{

	public MyFrameV8(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});

			}
			public static void createAndShowGUI() {
				MyFrameV8 msw = new MyFrameV8("My Frame V8");
				msw.addComponents();
				msw.setFrameFeatures();
			}
			
			protected void addComponents() {
				add(new MyCanvasV8());
			}
			
			protected void setFrameFeatures() {
				pack();
				setLocationRelativeTo(null);
				setVisible(true);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			}

}

