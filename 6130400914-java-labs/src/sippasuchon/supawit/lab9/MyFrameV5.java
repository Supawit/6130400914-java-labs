/* This program creates Frame name "My Frame4" extends MyFrame4.
 * Create object from MyCanvas5 class.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import sippasuchon.supawit.lab8.MyCanvasV4;
import sippasuchon.supawit.lab8.MyFrameV4;

public class MyFrameV5 extends MyFrameV4 {

	public MyFrameV5(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});

			}
			public static void createAndShowGUI() {
				MyFrameV5 msw = new MyFrameV5("My Frame V5");
				msw.addComponents();
				msw.setFrameFeatures();
			}
			
			protected void addComponents() {
				add(new MyCanvasV5());
			}
			
			protected void setFrameFeatures() {
				pack();
				setLocationRelativeTo(null);
				setVisible(true);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			}

}
