/* This program draws 2D Graphic.
 * Extends MyCanvasV4 and implements Runnable.
 * Move ball from x = 0 to x = WIDTH.
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import sippasuchon.supawit.lab8.MyBall;
import sippasuchon.supawit.lab8.MyCanvas;
import sippasuchon.supawit.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4  implements Runnable{
	
	MyBallV2 ball = new MyBallV2(0, HEIGHT/2 - MyBall.diameter/2);
	Thread running = new Thread(this);
	
	public MyCanvasV5() {
		super();
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public void run() {
		while(true) {
		// TODO Auto-generated method stub
			if (ball.x + MyBall.diameter > WIDTH) {
				break;
			} else {
				ball.move();
			}
					
			repaint();
		
			try{
				Thread.sleep(10);
			} catch (InterruptedException ex) {
			
			}
		
		}
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, super.WIDTH, super.HEIGHT);
		g2d.setColor(Color.WHITE);  
		g2d.fill(ball);
	}
}
