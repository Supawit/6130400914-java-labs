/* This Class extends MyBall.
 * There is 1 method
 * Method move() change x and y when use this method.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import sippasuchon.supawit.lab8.MyBall;

public class MyBallV2 extends MyBall {
	protected int ballVelX ;
	protected int ballVelY ;
	
	public MyBallV2(double arg0, double arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	
    public void move() {
    	x += ballVelX;
    	y += ballVelY;
    }
    

}
