/* This program creates Frame name "My Frame6" extends MyFrame5.
 * Create object from MyCanvas6 class.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:April 07 2019
 */
package sippasuchon.supawit.lab9;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV6 extends MyFrameV5{

	public MyFrameV6(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});

			}
			public static void createAndShowGUI() {
				MyFrameV6 msw = new MyFrameV6("My Frame V6");
				msw.addComponents();
				msw.setFrameFeatures();
			}
			
			protected void addComponents() {
				add(new MyCanvasV6());
			}
			
			protected void setFrameFeatures() {
				pack();
				setLocationRelativeTo(null);
				setVisible(true);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			}

}
