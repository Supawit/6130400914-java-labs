package sippasuchon.supawit.lab5;
/**
 * AndroidDevice is abstract class.
 * AndroidDevice can't build object from this class.
 *  
 * 
 * @author Supawit Sippasuchon
 * @version 1.0
 * @since 02.18.2019
 *
 *
 */
public abstract class AndroidDevice {
	private static String os = "Android"; 
	/**
	 * usage is abstract method
	 */
	public void usage() {
		System.out.println("LoL");
	}
	public static String getOs() {
		return os;
	}
	public static void setOs(String os) {
		AndroidDevice.os = os;
	}
	
}