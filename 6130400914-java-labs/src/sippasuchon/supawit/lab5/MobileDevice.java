package sippasuchon.supawit.lab5;

import java.io.Serializable;

/**
 * 
 * MobileDevice is to model a mobile device object with
 * attributes modelName, os, price, and weight.
 * 
 * @author Supawit Sippasuchon
 * @version 1.0
 * @since 02.18.2019
 *
 */
public class MobileDevice implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8544066553522757371L;
	
	private String modelName;
	private String os;
	private int price;
	private int weight;
	
	public String getModelName() {
		return modelName;
	}
	
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	public String getOs() {
		return os;
	}
	
	public void setOs(String os) {
		this.os = os;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public MobileDevice(String modelName, String os, int price, int weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}
	
	public MobileDevice(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = 0;
	}
	
	@Override
	public String toString() {
		return "MobileDevice [model name:" + modelName + ", OS: " + os + ", Price: " + price + " Baht, Weight: " + weight + " g]";
	}
	
}
