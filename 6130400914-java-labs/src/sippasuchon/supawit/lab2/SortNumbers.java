/*This program accepts 5 arguments:number1, 
 * number2, number3, number4, number5.
 *
 * Use Float.parseFloat() to convert a String to float.
 * 
 * import java.util.Arrays for use Arrays.sort().
 * 
 * Arrays.sort(array) can sort information.
 * 
 * Use for loop to output sort number.
 * 
 * If program don't gets 4 arguments. 
 * program will display 
 * "SortNumbers <number1> <number2> <number3> <number4> <number5> " in red letter.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:January 22 2019
 */
package sippasuchon.supawit.lab2;

import java.util.Arrays;
public class SortNumbers {

	public static void main(String[] args) {
		if (args.length == 5) {
			float number1, number2, number3, number4, number5;	
			number1 = Float.parseFloat(args[0]);
			number2 = Float.parseFloat(args[1]);
			number3 = Float.parseFloat(args[2]);
			number4 = Float.parseFloat(args[3]);
			number5 = Float.parseFloat(args[4]);
			float [] array = {number1, number2, number3, number4, number5};
			Arrays.sort(array);
			for (int i = 0; i < array.length; i++) {
				   System.out.print(array[i] + " ");
			}
		} else {
			System.err.println("SortNumbers <number1> <number2> <number3> <number4> <number5> ");
		}
	}
}
