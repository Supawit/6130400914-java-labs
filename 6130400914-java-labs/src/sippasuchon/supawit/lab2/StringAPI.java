/*This program accepts only 1 argument: the name of school.
 * By use String Method substing(Int) to cut String 
 *  except some String("college","university") you want to use.
 * Use equalsIgnoreCase("String") to compare two String by ignore cases.
 * 
 * The output of the program is in the format 
 * <schoolName> + " is a college" when String has "college" word at tail.
 * 
 * The output of the program is in the format 
 * <schoolName> + " is a university" when String has "university" word at tail.
 * 
 * The output of the program is in the format
 * <schoolName> + " is neither a university nor a college" 
 *  when String does't have "university" and "college" words at tail.
 * 
 * If program don't gets 1 arguments. 
 * program will display 
 * "StringAPI <schoolName>" in red letter.
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:January 22 2019
 */
package sippasuchon.supawit.lab2;

public class StringAPI {

	public static void main(String[] args) {
		if (args.length == 1) {
		
			String  schoolName, schoolTypeCollege, schoolTypeUniversity ; 
			schoolName = args[0];
			schoolTypeCollege = schoolName.substring(schoolName.length() - 7);
			schoolTypeUniversity = schoolName.substring(schoolName.length() - 10);
			if (schoolTypeCollege.equalsIgnoreCase("college")) {
				System.out.println(schoolName + " is a college");
			
			} else if (schoolTypeUniversity.equalsIgnoreCase("university")) {
				System.out.println(schoolName + " is a university");
				
			} else {
				System.out.println(schoolName + " is neither a university nor a college");
				
			}
		} else {
			System.err.println("StringAPI <schoolName>");
		}
	}

}
