package sippasuchon.supawit.lab6;

import java.awt.*;

import javax.swing.*;

public class MobileDeviceFormV1 extends MySimpleWindow {
	
	protected JPanel panelTop;
	
	public MobileDeviceFormV1(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFrom1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFrom1.addComponents();
		mobileDeviceFrom1.setFrameFeatures();	
	}
	
	protected void addComponents() {
		super.addComponents();
		JLabel brand = new JLabel("Brand Name:");
		JLabel model = new JLabel("Model Name:");
		JLabel weight = new JLabel("Weight (kg.):");
		JLabel height = new JLabel("Height (metre):");
		JLabel os = new JLabel("Mobile OS:");
		JTextField brandText = new JTextField(15);
		JTextField modelText = new JTextField(15);
		JTextField weightText = new JTextField(15);
		JTextField heightText = new JTextField(15);
		JRadioButton android = new JRadioButton("Android");
		JRadioButton ios = new JRadioButton("iOS");
		ButtonGroup group = new ButtonGroup();
		group.add(android);
		group.add(ios);
		
		panelTop = new JPanel(new GridLayout(0, 2, 0, 5));
		panelTop.add(brand);
		panelTop.add(brandText);
		panelTop.add(model);
		panelTop.add(modelText);
		panelTop.add(weight);
		panelTop.add(weightText);
		panelTop.add(height);
		panelTop.add(heightText);
		
		JPanel panelTopEast = new JPanel();
		JPanel panelTopWest = new JPanel();
		panelTopWest.setLayout(new GridLayout(1, 2));
		panelTopWest.add(os);
		panelTopEast.add(android);
		panelTopEast.add(ios);
		
		panelTop.add(panelTopWest);
		panelTop.add(panelTopEast);
		super.add(panelTop, BorderLayout.NORTH);

		}
}
