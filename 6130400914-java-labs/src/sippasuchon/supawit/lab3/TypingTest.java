/**
 * This program accepts only 1 argument: name of eight colors.
 * Program random 8 colors and display on console.
 * When you start program.Program will start to count time.
 * User must type in the same 8 random colors.
 * The answer is ignore case.User can input head as "Red", "RED" or "red"
 * By uses Java class Scanner and method next() for input. 
 * If the user type in incorrectly.User needs to type in again.
 * If the user type in correctly.Program will stop to count time.
 * If time less than or equal 12 seconds.User types faster than average.
 * If time more than 12 seconds.User types slower than average. 
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 02 2019
 */
package sippasuchon.supawit.lab3;
import java.util.Scanner;
/**
 * 
 * @author Supawit Sippasuchon
 * @version 1.0,02.02.2019
 *
 */
public class TypingTest {
	/**
	 * The main method of the program
	 * @param args arguments don't use
	 */

	public static void main(String[] args) {
		int i = 1;
		String allCorlor = "";
		while (i <= 8) {
			i += 1;
			String corlor = randomColor();
			if (i <= 8) {
				allCorlor += corlor + " ";
			} else {
				allCorlor += corlor;
			}
		}
		System.out.print(allCorlor + "\n");
		double startTime, endTime, allTime;
		startTime = System.currentTimeMillis();
		while(true) {
			System.out.print("Type your answer: ");
			Scanner answer = new Scanner(System.in);
			String yourAnswer = answer.nextLine();
			if (allCorlor.equalsIgnoreCase(yourAnswer)){
				endTime = System.currentTimeMillis();
				allTime = ( endTime - startTime)/1000;
				System.out.println("Your time is: " + allTime + ".");
				if (allTime <= 12) {
					System.out.println("You type faster than average person");
				} else {
					System.out.println("You type slower than average person");
				}
				System.exit(0);
				
				
			}
		}
	}
	/**
	 * This method uses for random 8 colors
	 * @return random color
	 */
	public static String randomColor() {
		int random = (int)(Math.random() * 8 + 1);
		switch (random)
		{
		 case 1 : //RED
			 return "RED";
		 case 2 : //ORANGE
			 return "ORANGE";
		 case 3 : //YELLOW
			 return "YELLOW";
		 case 4 : //GREEN
			 return "GREEN";
		 case 5 : //BLUE
			 return "BLUE";
		 case 6 : //INDIGO
			 return "INDIGO";
		 case 7 : //VIOLET
			 return "VIOLET";
		 default :	 //WHITE
			 return "WHITE";
		}
		
	}
	
}
