/**
 * This program accepts arguments As much as user wants but only number by first letter must be integer:
 * First letter uses for check the number of entered numbers is the same as the first integer entered.
 * If not, an error message will be displayed. 
 * If the program arguments are entered correctly.
 * Program will uses all argument except first letter to calculates for find the minimum, the maximum, 
 *  the average and the standard deviation of the numbers.
 *  
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 02 2019
 */
package sippasuchon.supawit.lab3;
import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * 
 * @author Supawit Sippasuchon
 * @version 1.0,02.02.2019
 *
 */
public class BasicStat {
	
	static double num[];
	static int numInput;
	/**
	 * The main method of the program
	 * @param args numbers only Ex 1.1, 2
	 */
	public static void main(String[] args) {
		acceptInput(args);
		displayStats();	
	}
	/**
	 * This method adds one String[] for check numbers
	 * @param args numbers only Ex 1.1, 2
	 */
	public static void acceptInput(String[] args) {
		num = new double[args.length - 1];
		numInput = Integer.parseInt(args[0]);
		if (numInput == (args.length - 1) ) {
			for(int i = 0; i < args.length - 1; i++) {				
				num[i] = Double.parseDouble(args[i + 1]);		
			}
		} else {
			System.err.println("<BasicStat> <numNumbers> <numbers>...");
			System.exit(1);
		}
	}	
	/**
	 * This method calculates to find the minimum, the maximum, the average and the standard deviation of the numbers
	 */
	public static void displayStats(){
		double max, min, averageSum, average, standardDeviation, u, xiMinusU;
		max = num[0];
		min = num[0];
		averageSum = 0;
		standardDeviation = 0;
		xiMinusU = 0;
		u = 0;
		for (int i = 0; i < num.length; i++) {
			if (num[i] >= max) {
				max = num[i];
			}
			if (num[i] <= min) {
				min = num[i];	
			}
			averageSum += num[i];	
		}
		average = averageSum / num.length;
		u = averageSum / num.length;
		for (int i = 0; i < num.length; i++) {
			xiMinusU += Math.pow((num[i] - u), 2);
		}
		standardDeviation = Math.sqrt(xiMinusU / num.length);
		
		System.out.print("Max is " + max);
		System.out.println(" Min is " + min);
		System.out.println("Average is " + average);
		System.out.println("Standard Deviation is " + standardDeviation);
	}
}
