package sippasuchon.supawit.lab5;
/**
 *
 * AndroidSmartWatch is MobileDevice's subclass.
 * 
 * @author Supawit Sippasuchon
 * @version 1.0
 * @since 02.18.2019
 *
 */
public class AndroidSmartWatch extends AndroidDevice {
	private String modelName;
	private String brandName;
	private int price;
	@Override
	/**
	 * method from AndroidDevice abstract class
	 */
	public void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, data, your heart rate, and your step count.");
	}
	/**
	 * Constucter 3 inputs brandName, modelName, price
	 * @param brandName name of brand
	 * @param modelName name of model
	 * @param price name of price
	 * 
	 */
	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "AndroidSmartWatch [BrandName:" + brandName + ", Model Name:" + modelName + ", price:" + price + " Baht]";
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	/**
	 * This method displays text "Display time only using a digital format"
	 */
	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}
	
}
