package sippasuchon.supawit.lab5;
/**
 * 
 * SamsungDevice is to model a mobile device object with
 * attributes modelName, price, weight and roidVersion.
 * SamsungDevice is MobileDevice's subclass.
 * 
 * @author Supawit Sippasuchon
 * @version 1.0
 * @since 02.18.2019
 *
 */
public class SamsungDevice extends MobileDevice{
	private static String brand = "Samsung";
	private static String modelName ;
	private Double androidVersion;
	@Override
	/**
	 * This toString reused from MobileDevice by using substing method.
	 */
	public String toString() {
		String firstbit = super.toString().substring(6 ,super.toString().length() - 1);                    
		return  "Samsung" + firstbit + ", " + getOs() + " version:" + getAndroidVersion() + "]";
		

	}
	public static String getBrand() {
		return brand;
	}
	
	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}
	
	public Double getAndroidVersion() {
		return androidVersion;
	}
	
	public void setAndroidVersion(Double androidVersion) {
		this.androidVersion = androidVersion;
	}
	
	public SamsungDevice(String modelName, int price, Double androidVersion) {
		super(modelName, "Android", price, 0);
		setAndroidVersion(androidVersion);
	}
	
	public SamsungDevice(String modelName, int price, int weight, Double androidVersion) {
		super(modelName, "Android", price, weight);
		setAndroidVersion(androidVersion);
	}
	/**
	 * This method displays text "Display times in both using a digital format and using an analog watch"
	 */
	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
	}
}
