/**
 * This program accepts only 1 argument: choice between head or tail.
 * By uses Java class Scanner and method next() for input.
 * The answer ignore case.User can input head as "HeaD", "HEAD" or "head"
 * If the answer is entered incorrectly an error message will be showed.
 * Computer will generate a random choice head or tail.
 * If you answer coincide a random computer choice.You win.
 * But If you answer don't coincide a random computer choice.Computer win.
 * The program continuously run until the user enter the choice "exit".
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 02 2019
 */
package sippasuchon.supawit.lab3;

import java.util.Scanner;
/**
 * 
 * @author Supawit Sippasuchon
 * @version 1.0,02.02.2019
 * 
 */

public class MatchingPennyMethod {
	/**
	 * The main method of the program
	 * @param args 
	 */
	public static void main(String[] args) {
		while(true) {
			
			Scanner inputKey = new Scanner(System.in);
			System.out.print("Enter head or tail: ");
	        String headOrTail = inputKey.nextLine();
	        String lowString = headOrTail.toLowerCase();
	        String playerChoice = acceptInput(lowString);
	        if ((lowString.equals("exit"))){
	        	System.out.println("Good Bye");
	        	System.exit(0);
	        }
	        if (playerChoice.equals("wrong")){
	        	System.err.println("Incorrect input. head or tail only");
	        	continue;	        
	        }
	        System.out.println("You play " + playerChoice);
	        String comChoice = genComChoice();
	        System.out.println("Compuer play " + comChoice);
	        displayWiner(comChoice, playerChoice);
		}
	}
	/**
	 * This method adds 1 String
	 * @param yourPenny String that you key on keyboard
	 * @return head,tail or wrong
	 */
	public static String acceptInput(String yourPenny) {
		switch (yourPenny)
		{
		 case "head" :			 	
			 return "head";
		 case "tail" :			 			 
			 return "tail";
		 default :	
			 return "wrong";
		}
	}
	/**
	 * This method is random choice from computer
	 * @return head or tail
	 */
	public static String genComChoice(){
		int random;
		random = (int)(Math.random() * ((0.999999999999999) + 1));
		if (random == 1) { 
			return "head";	
		} else {				
			return "tail";
		}
	}
	/**
	 * This method adds two strings
	 * @param com random choice from computer
	 * @param player user choice
	 */
	public static void displayWiner(String com,String player) {
		
		if (player.equals(com)) {
		 
			 System.out.println("You win.");
		} else {		 	
			 System.out.println("Computer wins.");
		}
	}	
		
	
	
	
}
