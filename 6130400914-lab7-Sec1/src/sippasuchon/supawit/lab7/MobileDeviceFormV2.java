package sippasuchon.supawit.lab7;
/**
 * This program extends from MobileDeviceFormV1.
 * 1. The title of the program is "Mobile Device Form V2" .
 * 2. The program has the label "Type:" and the combo box of device type 
 * 	 which includes "Phone", "Tablet", "Smart TV" and allow the user to edit this combo box. 
 * 3. The program has the label "Review:" ,the text area with 3 rows and 35 columns with the scroll pane.
 * 4. Initialize the text area with the content as "Bigger than previous Note phones in every way, 
 * 	 the Samsung Galaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh battery, and a massive 1TB of storage option. "
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 28 2019
 * 
 * 
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	protected JPanel panelMid;
	protected JLabel review;
	protected JTextArea reviewTextArea;
	protected JLabel type;

	public MobileDeviceFormV2(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceFrom2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceFrom2.addComponents();
		mobileDeviceFrom2.setFrameFeatures();	
	}
	
	protected void addComponents() {
		super.addComponents();
		type = new JLabel("Type:");
		JComboBox phoneBox = new JComboBox();
		phoneBox.addItem("Phone");
		phoneBox.addItem("Tablet");
		phoneBox.addItem("Smart TV");
		phoneBox.setEditable(true);
		panelTop.add(type);
		panelTop.add(phoneBox);
		
		panelMid = new JPanel(new BorderLayout()); 
		review = new JLabel("Review:"); 
		panelMid.add(review, BorderLayout.CENTER);
		reviewTextArea = new JTextArea(3, 35);
		reviewTextArea.setRows(2);
		reviewTextArea.setLineWrap(true);
		reviewTextArea.setWrapStyleWord(true);
		reviewTextArea.setText("Bigger than previous Note phones in every way, "
				+ "the Samsung Galaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh battery, and a massive 1TB of storage option. ");
		JScrollPane reviewSP = new JScrollPane(reviewTextArea);
		panelMid.add(reviewSP, BorderLayout.SOUTH);
		
		super.add(panelMid, BorderLayout.CENTER);
		
	}
}
