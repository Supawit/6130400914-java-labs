package sippasuchon.supawit.lab7;

import javax.swing.*;
import java.awt.*;
import java.applet.*;


public class MobileDeviceFormV5 extends MobileDeviceFormV4  {
	private static final Font labelsFont = new Font("Serif", Font.PLAIN, 14);
	private static final Font textFieldFont = new Font("Serif", Font.BOLD, 14);
	public MobileDeviceFormV5(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceFrom5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceFrom5.addComponents();
		mobileDeviceFrom5.addMenus();
		mobileDeviceFrom5.setFrameFeatures();	
	}
	
	protected void initComponents() {
		brandText.setFont(textFieldFont);
		modelText.setFont(textFieldFont);
		weightText.setFont(textFieldFont);
		heightText.setFont(textFieldFont);
		brand.setFont(labelsFont);
		model.setFont(labelsFont);
		weight.setFont(labelsFont);
		height.setFont(labelsFont);
		os.setFont(labelsFont);
		review.setFont(labelsFont);
		reviewTextArea.setFont(textFieldFont);
		type.setFont(labelsFont);
		feature.setFont(labelsFont);
		cancelBut.setForeground(Color.RED);
		okBut.setForeground(Color.BLUE);
	}
	
	protected void addComponents() {
		super.addComponents();	
		initComponents();
	}
}
