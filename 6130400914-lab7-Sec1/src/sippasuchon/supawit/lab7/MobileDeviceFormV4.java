package sippasuchon.supawit.lab7;

import javax.swing.*;


public class MobileDeviceFormV4 extends MobileDeviceFormV3{

	public MobileDeviceFormV4(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceFrom4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceFrom4.addComponents();
		mobileDeviceFrom4.addMenus();
		mobileDeviceFrom4.setFrameFeatures();	
	}
	
	protected void addComponents() {
		super.addComponents();
	}
	
	protected void updateMenuIcon() {
		newItem.setIcon(new ImageIcon("images/new.png"));
		/**menuFile.remove(newItem);
		newItem = new JMenuItem("New", new ImageIcon("images/new.png"));
		menuFile.add(newItem);
		menuFile.add(openItem);
		menuFile.add(saveItem);
		menuFile.add(exitItem);
		*/
	}
	
		protected void addMenus(){
		super.addMenus();
		updateMenuIcon();
		addSubMenus();
	}
		
	protected void addSubMenus() {
		menuConfig.remove(colorItem);
		menuConfig.remove(sizeItem);
		
		JMenu subCorlorMenu = new JMenu("  Color");
		JMenuItem redItem = new JMenuItem("  Red");
		JMenuItem greenItem = new JMenuItem("  Green");
		JMenuItem blueItem = new JMenuItem("  Blue");
		subCorlorMenu.add(redItem);
		subCorlorMenu.add(greenItem);
		subCorlorMenu.add(blueItem);
		menuConfig.add(subCorlorMenu);
		
		JMenu subSizeMenu = new JMenu("  Size");
		JMenuItem number16Item = new JMenuItem("  16");
		JMenuItem number20Item = new JMenuItem("  20");
		JMenuItem number24Item = new JMenuItem("  24");
		subSizeMenu.add(number16Item);
		subSizeMenu.add(number20Item);
		subSizeMenu.add(number24Item);
		menuConfig.add(subSizeMenu);
	}
}
