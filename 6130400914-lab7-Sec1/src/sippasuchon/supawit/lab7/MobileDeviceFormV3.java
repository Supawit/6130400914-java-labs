package sippasuchon.supawit.lab7;
/**
 * This program extends from MobileDeviceFormV2.
 * 1. The title of the program is "Mobile Device Form V3" .
 * 2. The program has the list with the label "Features:" and the choices as 
 *      "Design and build quality", "Great Camera", "Screen", "Battery Life".
 * 3. Menu "File" has four menu items which are "New", "Open", "Save", and "Exit".
 * 4. Menu "Config" has two menu items which are "Color", and "Size".
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 28 2019
 * 
 * 
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;


public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	
	protected JMenu menuFile;
	protected JMenuItem newItem;
	protected JMenuItem openItem;
	protected JMenuItem saveItem;
	protected JMenuItem exitItem;
	protected JMenu menuConfig;
	protected JMenuItem colorItem;
	protected JMenuItem sizeItem;
	protected JLabel feature;
	
	public MobileDeviceFormV3(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceFrom3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceFrom3.addComponents();
		mobileDeviceFrom3.addMenus();
		mobileDeviceFrom3.setFrameFeatures();	
		
	}
	
	protected void addComponents() {
		super.addComponents();
		feature = new JLabel("Features:");
		String label[] = {"Design and build quality", "Great Camera", 
				"Screen", "Battery Life"};	
		JList labelList = new JList(label);
		JPanel midTop = new JPanel(new GridLayout(0, 2));
		midTop.add(feature);
		midTop.add(labelList);
		
		panelMid.add(midTop, BorderLayout.NORTH);
		
	}
	
	protected void addMenus() {
		JMenuBar menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		menuConfig = new JMenu("Config");
		menuBar.add(menuFile);
		menuBar.add(menuConfig);
		
		newItem = new JMenuItem("New");
		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem = new JMenuItem("Exit");
		menuFile.add(newItem);
		menuFile.add(openItem);
		menuFile.add(saveItem);
		menuFile.add(exitItem);
		
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");
		menuConfig.add(colorItem);
		menuConfig.add(sizeItem);
	
		super.setJMenuBar(menuBar);
	}
		
	

}
