/*This program accepts four arguments:number of 1000 baht,
 *  number of 500 baht, number of 100 baht, number of 20 baht.
 * 
 * Use Integer.parseInt() to convert a String to int.
 * 
 * totalMoney is "total of money"
 * the output of the program is in the format 
 * "Total money is " + totalMoney 
 * 
 * If program don't gets 4 arguments. 
 * program will display 
 * "ComputerMoney <1000 Baht> <500 Baht> <100 Baht> <20 Baht>" in red letter.
 *
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:January 22 2019
 */
package sippasuchon.supawit.lab2;

public class ComputerMoney {

	public static void main(String[] args) {
		if (args.length == 4) {
			int numberOneThousand, numberFiveHundred, numberOneHundred, numberTwenty  ;
			float totalMoney;
				numberOneThousand = Integer.parseInt(args[0]);
				numberFiveHundred = Integer.parseInt(args[1]);
				numberOneHundred = Integer.parseInt(args[2]);
				numberTwenty =Integer.parseInt(args[3]);
				totalMoney = (numberOneThousand * 1000) + (numberFiveHundred * 500) + (numberOneHundred * 100) + (numberTwenty * 20);
				System.out.println("Total money is " + totalMoney);
		} else {
				System.err.println("ComputerMoney <1000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		}
	}
}
			

	