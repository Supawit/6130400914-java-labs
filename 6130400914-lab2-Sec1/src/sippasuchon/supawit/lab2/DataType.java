/*This program have 9 conditions.
 * 1.display "My name is " + <my name>.
 * 2.display "My student ID is " + <my ID>.
 * 3.use String Method char.At(int index) to get first letter.
 * 4.(3 > 2) so answer is ture .
 * 5. and 6. use parseInt() Method change lastTwoMyIDOctal or
 *  lastTwoMyIDHexa to myRealID.
 * 7.use long variable to display myId.
 * 8.use float variable to display myId ,(14.61f) f means float.
 * 9.use double variable to display myId. 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:January 21 2019
 */
package sippasuchon.supawit.lab2;

public class DataType {

	public static void main(String[] args) {
		//1
		String myName = "Supawit Sippasuchon";
		System.out.println("My name is " + myName);
		//2
		String myId = "6130400914";
		System.out.println("My student ID is " + myId);
		//3
		char firstLetterName = myName.charAt(0);
		System.out.print(firstLetterName + " ");
		//4
		int number = 3;
		boolean answer = (number > 2);
		System.out.print(answer + " ");
		//5
		String oc = "16";
		int myIdOctal = Integer.parseInt(oc,8);
		System.out.print(myIdOctal + " ");
		//6
		String hex = "E";
		int myIdHexa = Integer.parseInt(hex,16);
		System.out.println(myIdHexa);
		//7
		long lastTwoLong = 14;
		System.out.print(lastTwoLong + " ");
		//8
		float lastTwoFloat = 14.61f;
		System.out.print(lastTwoFloat + " ");
		//9
		double lastTwoDouble = 14.61;
		System.out.print(lastTwoDouble);
	}
}
