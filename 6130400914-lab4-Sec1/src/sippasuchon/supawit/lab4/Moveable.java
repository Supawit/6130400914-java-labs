package sippasuchon.supawit.lab4;

public interface Moveable {
	void accelerate();
	void brake();
	abstract void setSp(int speed);

}
