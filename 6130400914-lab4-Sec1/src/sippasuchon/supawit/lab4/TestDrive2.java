/** This class extends Automobile.
 * Method isFaster(varible1, varible2) compares if the first automobile is faster than the second automobile
 * If isFaster(car1, car2),method isFaster(ToyotaAuto car1, HondaAuto car2) will be uesd.
 * But if isFaster(car2, car1),method isFaster(HondaAuto car2, ToyotaAuto car1)will be used.
 * 
 *
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 08 2019
 */
package sippasuchon.supawit.lab4;

public class TestDrive2 extends Automobile {
	public static void isFaster(ToyotaAuto car1, HondaAuto car2){
		if (car1.getSpeed() > car2.getSpeed()) {
			System.out.println(car1.getModel() + " is faster than " + car2.getModel());
		} else {
			System.out.println(car1.getModel() + " is not faster than " + car2.getModel());
		}
	}
	
	public static void isFaster(HondaAuto car2, ToyotaAuto car1){
		if (car2.getSpeed() > car1.getSpeed()) {
			System.out.println(car2.getModel() + " is faster than " + car1.getModel());
		} else {
			System.out.println(car2.getModel() + " is not faster than " + car1.getModel());
		}
	}
	
	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		car2.accelerate();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
		isFaster(car2, car1);
		
	}

}
