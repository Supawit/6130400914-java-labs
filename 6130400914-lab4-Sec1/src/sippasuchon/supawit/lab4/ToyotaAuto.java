/** This class extends Automobile and implements interface Moveable, Refuelable.
 * Has one public constructor that accepts int maximum speed, int acceleration  and String the model.
 * Method refuel() sets the value of gasoline to 100 and displays a message "refuels".
 * Method accelerate() sets increases the current speed by the acceleration. 
 * 	If the new speed does not exceed the maximum speed. Otherwise set the new speed to the maximum speed.
 * 	Decreases gasoline by 15 and displays message "accelerates".
 * Method brake() sets decreases the current speed by the acceleration.
 * 	If the new speed is not less than 0. Otherwise set the new speed to 0.
 * 	Decreases the value of gasoline by 15 and displays message "brakes".
 * The method setSpeed(int speed) if the input speed must neither be negative nor exceed maxSpeed. 
 * Otherwise set speed to 0 or maxSpeed accordingly.
 * 
 * 
 * Author:Supawit Sippasuchon
 * ID:613040091-4
 * Sec:1
 * Data:February 08 2019
 */
package sippasuchon.supawit.lab4;

public class ToyotaAuto extends Automobile implements Moveable, Refuelable {

		@Override
		public void refuel() {
			setGasoline(100);
			System.out.println(getModel() + " refuels");
		}

		@Override
		public void accelerate() {
			setSpeed(getSpeed() + getAcceleration());
			setSp(getSpeed());
			setGasoline(getGasoline() - 15);
			System.out.println(getModel() + " accelerates");
		}

		@Override
		public void brake() {
			setSpeed(getSpeed() - getAcceleration());
			setSp(getSpeed());
			setGasoline(getGasoline() - 15);
			System.out.println(getModel() + " brakes");
		}

		@Override
		public void setSp(int speed) {
			if (speed < 0) {
				setSpeed(0);
			}
			if (speed > getMaxSpeed()) {
				setSpeed(getMaxSpeed());
			}
		}
	
	public String toString() {
		return getModel() + " gas:" + getGasoline() + " speed:" + getSpeed() + " max speed:" + 
				getMaxSpeed() + " acceleration:" + getAcceleration();
	}


	public ToyotaAuto(int maxSpeed,int acceleration, String model) {
		setAcceleration(acceleration);
		setMaxSpeed(maxSpeed);
		setModel(model);	
		setSpeed(0);
		setGasoline(100);
	}

}
